/*
 * Symboltable.cpp
 *
 *  Created on: Sep 26, 2012
 *      Author: knad0001
 */

#include "../../Utility/includes/SymTabEntry.h"
#include "../../Utility/includes/Token.h"
#include "../includes/Symboltable.h"
#include "../../Utility/includes/TokenType.h"
#include "../../Utility/includes/Information.h"

Symboltable::Symboltable() {
	entries = new SymTabEntry*[tableSize];

	for(int i = 0; i < tableSize; i++) {
		entries[i] = NULL;
	}

	initSymbols();
}

Symboltable::~Symboltable() {
	for (int i = 0; i < tableSize; i++) {
		if(entries[i] != NULL) {
			delete entries[i];
		}
	}
	delete[] entries;
}

void Symboltable::initSymbols() {
	this->addEntry("+", TokenType::PLUS, -1);
	this->addEntry("-", TokenType::MINUS, -1);
	this->addEntry("*", TokenType::STAR, -1);
	this->addEntry("!", TokenType::EXCLAMATIONMARK, -1);
	this->addEntry("(", TokenType::LBRACE, -1);
	this->addEntry(")", TokenType::RBRACE, -1);
	this->addEntry("=", TokenType::EQUAL, -1);
	this->addEntry("<", TokenType::LESS, -1);
	this->addEntry(">", TokenType::GREATER, -1);
	this->addEntry("{", TokenType::LCURLY, -1);
	this->addEntry("}", TokenType::RCURLY, -1);
	this->addEntry(";", TokenType::SEMICOLON, -1);
	this->addEntry("&&", TokenType::AND, -1);
	this->addEntry(":", TokenType::DP, -1);
	this->addEntry(":=", TokenType::DEF, -1);
	this->addEntry("[", TokenType::LECK, -1);
	this->addEntry("]", TokenType::RECK, -1);
	this->addEntry("=:=", TokenType::SPECIAL, -1);
	this->addEntry("if", TokenType::IF, -1);
	this->addEntry("IF", TokenType::IF, -1);
	this->addEntry("else", TokenType::ELSE, -1);
	this->addEntry("ELSE", TokenType::ELSE, -1);
	this->addEntry("while", TokenType::WHILE, -1);
	this->addEntry("WHILE", TokenType::WHILE, -1);
	this->addEntry("int", TokenType::INT, -1);
	this->addEntry("boolean", TokenType::BOOLEAN, -1);
	this->addEntry("void", TokenType::VOID, -1);
	this->addEntry("return", TokenType::RETURN, -1);
	this->addEntry("write", TokenType::WRITE, -1);
	this->addEntry("WRITE", TokenType::WRITE, -1);
	this->addEntry("read", TokenType::READ, -1);
	this->addEntry("READ", TokenType::READ, -1);
}

Information * Symboltable::getEntry(const char * lexem) {
	int bucket = hash(lexem) % tableSize;

	SymTabEntry * helpEntry = entries[bucket];

	while (helpEntry != NULL && strcmp(helpEntry->getLexem(), lexem) != 0) {
		helpEntry = helpEntry->getNextEntry();
	}
	if (helpEntry == NULL) {
		return NULL;
	} else {
		return helpEntry->getInformation();
	}
}

void Symboltable::addEntry(const char * lexem, TokenType type, int value) {
	int bucket = hash(lexem) % tableSize;

	if(this->getEntry(lexem) == NULL) {
		if(entries[bucket] == NULL) {
			Information * newInformation = new Information(lexem, type, value);
			newInformation->setNodeType(NodeType::UNDEFINED);
			SymTabEntry * newEntry = new SymTabEntry(lexem, newInformation);
			entries[bucket] = newEntry;
		} else {
			SymTabEntry * helpEntry = entries[bucket];
			while(helpEntry->getNextEntry() != NULL && strcmp(helpEntry->getLexem(), lexem) != 0) {
				helpEntry = helpEntry->getNextEntry();
			}
			if (helpEntry->getNextEntry() == NULL && strcmp(helpEntry->getLexem(), lexem) != 0) {
				Information * newInformation = new Information(lexem, type, value);
				SymTabEntry * newEntry = new SymTabEntry(lexem, newInformation);

				helpEntry->setNextEntry(newEntry);
			}
		}
	}
}

int Symboltable::hash(const char * lexem) {
	int seed = 131;//31  131 1313 13131131313 etc//
	int hash = 1;
	if(strlen(lexem) > 1){
		while(*lexem)
		{
			hash = (hash * seed) + (*lexem);
			lexem ++;
		}
	} else {
		hash = (hash * seed) + (*lexem);
	}


	return hash & (0x7FFFFFFF);
}

/*
 * Ist eigentlich unnötig und wurde durch strcmp() ersetzt. Vorsichtshalber lass ich es aber mal drin.
 */
bool Symboltable::isEqualString(const char * lexemEins, const char * lexemZwei) {
	while ((*lexemEins == *lexemZwei) && (*lexemEins != '\0')) {
		lexemEins++;
		lexemZwei++;
	}

	if (*lexemEins > *lexemZwei) {
		return 1;
	}
	if (*lexemEins < *lexemZwei) {
		return -1;
	}

	return true;
}

SymTabEntry ** Symboltable::returnEntries(){
	return entries;
}
