#include "../includes/Symboltable.h"
#include <iostream>

using namespace std;

int main(int argc, char **argv) {

	Symboltable* symtab = new Symboltable();

	symtab->addEntry("hallo", TokenType::AND, 1);
	symtab->addEntry("ciao", TokenType::BOOLEAN, 2);
	symtab->addEntry("hallo", TokenType::IDENTIFIER, 3);

	SymTabEntry ** entries = symtab->returnEntries();


	for(int i = 0; i < 1000; i++) {
		if(entries[i] != NULL) {
			cout << i << endl;
		}
	}

	if(strcmp("hallo", "hello") == 0) {
		cout << "gleich" << endl;
	} else {
		cout << "ungleich" << endl;
	}

	cout << symtab->getEntry("hallo")->getLexem() << "  " << symtab->getEntry("hallo")->getValue() << endl;
	cout << symtab->getEntry("ciao")->getLexem() << "  " << symtab->getEntry("ciao")->getValue() << endl;
	cout << symtab->getEntry("hallo")->getLexem() << "  " << symtab->getEntry("hallo")->getValue() << endl;

	cout << "hello" << endl;
	cout << "hello" << endl;
}
