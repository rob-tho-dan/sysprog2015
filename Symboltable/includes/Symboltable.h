/*
 * Symboltable.h
 *
 *  Created on: Sep 26, 2012
 *      Author: knad0001
 */

#ifndef SYMBOLTABLE_H_
#define SYMBOLTABLE_H_
#include "../../Utility/includes/Information.h"
#include "../../Utility/includes/Token.h"
#include "../../Utility/includes/SymTabEntry.h"

class Symboltable {
protected:
	int tableSize = 1000;
public:
	Symboltable();
	virtual ~Symboltable();

	void initSymbols();
	void addEntry(const char * lexem, TokenType type, int value);
	Information * getEntry(const char * lexem);

	int hash(const char * lexem);
	bool isEqualString(const char * lexemEins, const char * lexemZwei);

	SymTabEntry ** returnEntries();
private:
	SymTabEntry **entries;

};

#endif /* SYMBOLTABLE_H_ */
