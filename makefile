#
#	baut das komplette Scanner Projekt
#
OBJDIR = objs
INCLDIR = includes

AUTOMATDIR = Automat
BUFFERDIR = Buffer
SYMBOLTABLEDIR = Symboltable
SCANNERDIR = Scanner
UTILITYDIR = Utility
PARSERDIR = Parser

# rausgenommen
all:	automatOBJs symboltableOBJs bufferOBJs utilityOBJs scannerOBJs parserOBJs
	@echo "target all"

# rm 	remove
# -f	force, ohne nachfragen
clean:
	rm -f $(AUTOMATDIR)/$(OBJDIR)/*.o
	rm -f $(BUFFERDIR)/$(OBJDIR)/*.o
	rm -f $(SYMBOLTABLEDIR)/$(OBJDIR)/*.o
	rm -f $(SCANNERDIR)/$(OBJDIR)/*.o
	rm -f $(SCANNERDIR)/debug/*
	rm -f $(UTILITYDIR)/$(OBJDIR)/*.o
	rm -f $(AUTOMATDIR)/debug/*.o
	rm -f $(SYMBOLTABLEDIR)/debug/*.o
	rm -f $(BUFFERDIR)/debug/*.o
	rm -f $(PARSERDIR)/debug/*.o
	rm -f $(PARSERDIR)/$(OBJDIR)/*.o

utilityOBJs:
	$(MAKE) -C $(UTILITYDIR) UtilityOBJTarget

automatOBJs:
	$(MAKE) -C $(AUTOMATDIR) AutomatOBJTarget

bufferOBJs:
	$(MAKE) -C $(BUFFERDIR) BufferOBJTarget

symboltableOBJs:
	$(MAKE) -C $(SYMBOLTABLEDIR) SymboltableOBJTarget

scannerOBJs:
	$(MAKE) -C $(SCANNERDIR) makeTestScanner

parserOBJs:
	$(MAKE) -C $(PARSERDIR) makeTestParser
