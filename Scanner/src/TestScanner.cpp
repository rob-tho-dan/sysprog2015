#include "../../Buffer/includes/Buffer.h"
#include "../includes/Scanner.h"
#include "../includes/IScanner.h"
#include "../../Automat/includes/Automat.h"
#include "../includes/Scanner.h"
#include "../includes/ScannerImpl.h"

#include <fstream>

int main(int argc, char * argv[]) {
	Scanner * scanner;
	Buffer* buffer = 0;
	if(argc > 1) {
	    Buffer* buffer = new Buffer(argv[1]);

	    Symboltable * symtab = new Symboltable();

      //create instance of scannerimpl
      scanner = Scanner::create();
      //link buffer and symtab to scanner
      scanner->update(buffer, symtab);
      TokenType temp = TokenType::UNDEFINED;
			Token * token;

			ofstream myfile;
		  myfile.open ("out.txt");
			std::cout << "processing ..." << std::endl;
      while((token = scanner->nextToken())){
					if(token != NULL && token->getType() != TokenType::RETURN &&
					token->getType() != TokenType::COMMENT &&
					token->getType() != TokenType::ENDOFFILE) {
						token->printToken(&myfile);
					}

		      if(token != NULL && token->getType() == TokenType::ENDOFFILE){
							std::cout << "stop" << std::endl;
		          break;
		      }
      }
		  myfile.close();
			return EXIT_SUCCESS;
	} else {
		return EXIT_FAILURE;
	}


}
