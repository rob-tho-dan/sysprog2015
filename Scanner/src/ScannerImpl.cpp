#include <climits>
#include "../../Buffer/includes/Buffer.h"
#include "../../Utility/includes/Token.h"
#include "../../Utility/includes/Information.h"
#include "../includes/ScannerImpl.h"
#include "../../Symboltable/includes/Symboltable.h"
#include "../../Automat/includes/Automat.h"
#include <iostream>
#include <string.h>
#include <sstream>
#include <error.h>
#include <errno.h>

#define NULL __null

#include <iostream>
using namespace std;

Scanner * Scanner::create() {
	return (new ScannerImpl());
}

ScannerImpl::ScannerImpl(){
	tooMuchChars = 0;
	makeToken = false;
	row = 1;
	column = 1;
	tempType = TokenType::UNDEFINED;
	length = 0;

	char * temp = new char[2048];
	temp[0] = '\0';
	this->lexem = temp;

	currentLength = 0;
	currentChar = '\0';

	this->stop = false;
}

void ScannerImpl::update(Buffer * buffer, Symboltable * symtab) {
	this->buffer = buffer;
	this->symtab = symtab;
	this->automat = new Automat(this);
}

ScannerImpl::~ScannerImpl() {
/*
 * buffer und automat loeschen
 */
}

Scanner::~Scanner(){}

Token * ScannerImpl::nextToken() {
	if(stop){
		return new Token(TokenType::ENDOFFILE, nullptr, 0, 0);
	}

	makeToken = false;
	int currentRow = this->row;
	this->currentColumn = this->column;
	Information * tempInfo;

	length = 0;
    this->lexem = new char[1];
	this->lexem[0] = '\0';

	while(!makeToken){
		char tempChar = buffer->getChar();

		char * tempChar2 = &tempChar;
		currentChar = tempChar;

		if((tempChar == ' ' || tempChar == '\t') && this->column == this->currentColumn){
			currentColumn++;
		}

		automat->read(tempChar);

		if(tempChar != ' ') {
			if(tempChar == 0x03){
				stop = true;
			}
			//TODO hier funktioniert was nicht
			//TODO zu lange identifier stimmen nicht
			addChar(&tempChar);
			this->length++;
		}

		this->column++;

		if(this->tooMuchChars != 0) {
			//aus aktuellem lexem tooMuchChars-viele Buchstaben entfernen
			//removeChar in schleife aufrufen
			for(int i = 0; i < tooMuchChars; i++) {
				removeChar();
				this->column--;
				this->length--;
			}
		}
	}

	if(symtab->getEntry(lexem) == NULL) {
		if(tempType == TokenType::NUMBER){
			// hier schauen wie groß die zahl ist, wenn zu groß dann
			// errortoken bauen
			int calcValue = -1;
			calcValue = ScannerImpl::calcValue();
			if(calcValue != -1){
				symtab->addEntry(lexem, tempType, calcValue);
			} else {
				tempType = TokenType::ERROR;
				symtab->addEntry(lexem, tempType, calcValue);
			}
		} else if(tempType == TokenType::IDENTIFIER){
			//testen, dass Token nicht zu lang ! 2 hoch 11
			//length < 2048
			if(!checkIdentifier()){
				tempType = TokenType::ERROR;
			}

			symtab->addEntry(lexem, tempType, 0);
		} else {
			symtab->addEntry(lexem, tempType, 0);
		}
		tempInfo = symtab->getEntry(lexem);
	} else {
		tempInfo = symtab->getEntry(lexem);
		tempType = tempInfo->getTokenType();
	}

	//Dass die Zeile nur einmal hochgezaehlt wird.
	if(tempType == TokenType::RETURN){
		if(*lexem != '\r') {
			this->row++;
			this->column = 1;
		}
	}

	makeToken = false;
	tooMuchChars = 0;

	return new Token(tempType, tempInfo, currentRow, this->currentColumn);
}

void ScannerImpl::freeToken(Token token) {
	//removefunktion aus Symboltabelle ?!
}

void ScannerImpl::mkToken(TokenType type) {
	this->tempType = type;
	makeToken = true;
}

void ScannerImpl::returnInComment() {
	this->row++;
	this->column = 1;
}

void ScannerImpl::ungetChar(int amount) {
	this->tooMuchChars = amount;
	this->buffer->ungetChar(amount);
}

void ScannerImpl::addChar(char * singleChar) {
	size_t len = strlen(this->lexem);
  char* ret = new char[len+2];
  strcpy(ret, this->lexem);
  ret[len] = *singleChar;
  ret[len+1] = '\0';

	this->lexem = ret;
}

void ScannerImpl::removeChar() {
	int len1 = 0;
	if(this->lexem != nullptr){
		len1 = strlen(this->lexem);
		char * temp = (char *)malloc(len1 * sizeof(char));;
		for(int i = 0; i < len1 - 1; i++){
			temp[i] = this->lexem[i];
		}
		temp[len1 - 1] = '\0';
		this->lexem = temp;
	}
}

bool ScannerImpl::checkIdentifier() {
		if(this->length > 2048) {
			fprintf(stderr, "Error: Identifier zu lang. Row:%d Column:%d\n", this->row, this->currentColumn);
			exit(1);
			return false;
		}
		return true;
}

bool ScannerImpl::checkNumber(){
	stringstream str;
	str << this->lexem;
	int x;
	str >> x;

	if(str.fail()){
		fprintf(stderr, "Error: Integer ist zu groß. Row:%d Column:%d\n", this->row, this->currentColumn);
		exit(1);
	}

	return true;
}

int ScannerImpl::calcValue(){
	errno = 0;
  char *temp;
  long val = strtol(this->lexem, &temp, 0);
	if (temp == this->lexem || *temp != '\0' ||
      ((val == LONG_MIN || val == LONG_MAX) && errno == ERANGE))
      // fprintf(stderr, "Parse von long '%s' nicht moeglich!\n",
      //         this->lexem);
			return -1;
  return val;

}
