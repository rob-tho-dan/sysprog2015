#ifndef SCANNERIMPL_H_
#define SCANNERIMPL_H_
#include "../../Buffer/includes/Buffer.h"
#include "Scanner.h"
#include "IScanner.h"
#include "../../Symboltable/includes/Symboltable.h"
#include "../../Automat/includes/Automat.h"

class ScannerImpl: public Scanner, public IScanner {
public:
	ScannerImpl();
	~ScannerImpl();

	virtual void update(Buffer * buffer, Symboltable * symtab);

	virtual Token * nextToken();
	virtual void freeToken(Token token);

	virtual void mkToken(TokenType type);
	virtual void returnInComment();
	virtual void ungetChar(int amount);

	void addChar(char * singleChar);	//fuegt einen char hinzu
	void removeChar();	//entfernt einen char

	int calcValue();
	bool checkNumber();
	bool checkIdentifier();
private:
	Buffer * buffer;
	Symboltable * symtab;
	Automat * automat;
	int row;
	int column;
	TokenType tempType;
	char* lexem;
	int currentLength;
	bool makeToken;
	char lastChar;
	char currentChar;
	int tooMuchChars;
	int length;
	int currentColumn;

	bool stop;
};

#endif
