/*
 * Identifier.cpp
 *
 *  Created on: Apr 22, 2015
 *      Author: tom
 */

#include "../includes/IdentifierState.h"
#include <iostream>

void IdentifierState::read(char c, AutomatOO* automat){
	if( !((c >= 48) && (c <= 57)) && !((c >= 65) && (c <= 90)) &&
			!((c >= 97) && (c <= 122)) ){
		automat->mkToken(TokenType::IDENTIFIER);
		automat->setStateInitial();
		if(c != ' '){
			automat->ungetChar(1);
		}
	}


}
