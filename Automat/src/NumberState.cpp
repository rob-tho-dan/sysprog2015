/*
 * Number.cpp
 *
 *  Created on: Apr 22, 2015
 *      Author: tom
 */

#include "../includes/NumberState.h"
#include <iostream>


void NumberState::read(char c, AutomatOO* automat){
	if(!((48 <= c) && (c <= 57))){
		automat->mkToken(TokenType::NUMBER);
		automat->setStateInitial();
		if(c != ' '){
			automat->ungetChar(1);
		}
	}
}

// TODO End Of File hinzufuegen, dass der letzte toek nicht verschluckt wird
