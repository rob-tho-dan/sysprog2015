/*
 * CommentState.cpp
 *
 *  Created on: Apr 26, 2015
 *      Author: tom
 */

#include "../includes/CommentState.h"

void CommentState::read(char c, AutomatOO* automat){
	if(c == 0x03){
		automat->mkToken(TokenType::ERROR);
		automat->setStateInitial();
		automat->ungetChar(1);
	}
	if(c == '*'){
		flag = true;
	}

	if((c == ':') && flag){
		automat->mkToken(TokenType::COMMENT);
		automat->setStateInitial();
	} else if( c != '*'){
		if(c == '\n') {
			automat->returnInComment();
		}
		flag = false;
	}
}
