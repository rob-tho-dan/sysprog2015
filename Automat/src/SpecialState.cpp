/*
 * SpecialState.cpp
 *
 *  Created on: Apr 26, 2015
 *      Author: tom
 */

#include "../includes/SpecialState.h"
#include "../../Utility/includes/TokenType.h"

void SpecialState::read(char c, AutomatOO* automat){
	if(c == ':'){
		automat->setStateSpecial2();
	} else {
		automat->mkToken(TokenType::EQUAL);
		automat->setStateInitial();
		if(c != ' ' && c != '\0'){
			automat->ungetChar(1);
		}
	}
}


void SpecialState2::read(char c, AutomatOO* automat){
	if(c == '='){
		automat->mkToken(TokenType::SPECIAL);
		automat->setStateInitial();
	} else {
		automat->mkToken(TokenType::EQUAL);
		automat->setStateInitial();
		if(c != ' ' && c != '\0'){
			automat->ungetChar(2);
		}
	}
}
