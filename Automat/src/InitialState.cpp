/*
 * Initial.cpp
 *
 *  Created on: Apr 22, 2015
 *      Author: tom
 */

#include <iostream>

#include "../includes/InitialState.h"


void InitialState::read(char c, AutomatOO* automat){
	if((c >= 48) && (c <= 57)){
		automat->setStateNumber();
	} else if(((c >= 65) && (c <= 90)) || ((c >= 97) && (c <= 122))){
		automat->setStateIdentifier();
	} else {

		switch(c){
		case '+': // PLUS
			automat->mkToken(TokenType::PLUS);
			break;
		case '&': // AND
			automat->setStateAnd();
			break;
		case '-': // MINUS
			automat->mkToken(TokenType::MINUS);
			break;
		case ':': // COLON
			automat->setStateAsign();
			break;
		case '*': // STAR
			automat->mkToken(TokenType::STAR);
			break;
		case '<': // LESS
			automat->mkToken(TokenType::LESS);
			break;
		case '>': // GREATER
			automat->mkToken(TokenType::GREATER);
			break;
		case '=': // EQUAL
			automat->setStateSpecial();
			break;
		case '!': // EXCLAMATIONMARK
			automat->mkToken(TokenType::EXCLAMATIONMARK);
			break;
		case ';': // SEMICOLON
			automat->mkToken(TokenType::SEMICOLON);
			break;
		case '(': // LBRACE
			automat->mkToken(TokenType::LBRACE);
			break;
		case ')': // RBRACE
			automat->mkToken(TokenType::RBRACE);
			break;
		case '{': // LCURLY
			automat->mkToken(TokenType::LCURLY);
			break;
		case '}': // RCURLY
			automat->mkToken(TokenType::RCURLY);
			break;
		case '[': // LSQUARE
			automat->mkToken(TokenType::LECK);
			break;
		case ']': // RSQUARE
			automat->mkToken(TokenType::RECK);
			break;
		case '\n': // NEW LINE
			automat->mkToken(TokenType::RETURN);
			break;
		case '\r': // NEW LINE
			automat->mkToken(TokenType::RETURN);
			break;
		case 0x03: //TODO: EOF - Welcher char?
			automat->mkToken(TokenType::ENDOFFILE);
			break;
		case ' ':
			break;
		case '\t':
			break;
		default:
			automat->mkToken(TokenType::ERROR);
		}
	}

}
