/*
 * AndState.cpp
 *
 *  Created on: Dez 7, 2016
 *      Author: daniel
 */


#include "../includes/AndState.h"

void AndState::read(char c, AutomatOO* automat){
	if(c == '&'){
		automat->mkToken(TokenType::AND);
		automat->setStateInitial();
	} else {
		automat->mkToken(TokenType::ERROR);
  	automat->setStateInitial();
	}
}
