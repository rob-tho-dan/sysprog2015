/*
 * AssignState.cpp
 *
 *  Created on: Apr 25, 2015
 *      Author: tom
 */


#include "../includes/AsignState.h"

void AsignState::read(char c, AutomatOO* automat){
	if(c == '='){
		automat->mkToken(TokenType::DEF);
		automat->setStateInitial();
	}
	else if (c == '*') {
		automat->setStateComment();
	} else {
		automat->mkToken(TokenType::DP);
		if(c != ' ' && c != 0x03){
			automat->ungetChar(1);
		}
		automat->setStateInitial();
	}
}
