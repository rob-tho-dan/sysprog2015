/*
 * CommentState.h
 *
 *  Created on: Apr 26, 2015
 *      Author: tom
 */

#ifndef COMMENTSTATE_H_
#define COMMENTSTATE_H_

#include "State.h"

class CommentState : public State {
public:
	virtual ~CommentState(){};
	virtual void read(char,AutomatOO*);
private:
	bool flag;
};



#endif /* COMMENTSTATE_H_ */
