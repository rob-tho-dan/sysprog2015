/*
 * SpecialState.h
 *
 *  Created on: Apr 26, 2015
 *      Author: tom
 */

#ifndef SPECIALSTATE_H_
#define SPECIALSTATE_H_

#include "State.h"

class SpecialState : public State {
public:
	virtual ~SpecialState(){};
	virtual void read(char,AutomatOO*);
};

class SpecialState2 : public State {
public:
	virtual ~SpecialState2(){};
	virtual void read(char,AutomatOO*);
};


#endif /* SPECIALSTATE_H_ */
