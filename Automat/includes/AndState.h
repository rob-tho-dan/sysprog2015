/*
 * AndState.h
 *
 *  Created on: Dez 7, 2016
 *      Author: daniel
 */

#ifndef ANDSTATE_H_
#define ANDSTATE_H_

#include "State.h"

class AndState : public State {
public:
	virtual ~AndState(){};
	virtual void read(char,AutomatOO*);
};




#endif /* ANDSTATE_H_ */
