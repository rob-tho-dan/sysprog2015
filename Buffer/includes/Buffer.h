/*
 * Buffer.h
 *
 *  Created on: Sep 26, 2012
 *      Author: knad0001
 */

#ifndef BUFFER_H_
#define BUFFER_H_

class Buffer {
public:
	Buffer(char * file);
	char getChar();
	void ungetChar(int amount);
	void readTextData(bool x);
	virtual ~Buffer();
private:
	char *bufferA;
	char *bufferB;
	int fd;
	int byte_count;
	int byte_count2;
	char *zeiger;
};

#endif /* BUFFER_H_ */
