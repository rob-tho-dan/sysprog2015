/*
 * Buffer.cpp
 *
 *  Created on: Sep 26, 2012
 *      Author: knad0001
 */
#include "../includes/Buffer.h"
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <iostream>
#define BUFSIZE 512


using namespace std;
unsigned int counter = 0;
bool reload = false;
fstream fin;
int buffer_ptr; //0 -> Buffer A, 1 -> Buffer B
bool readAgain = true;

Buffer::Buffer(char * file) {

	fd = 0;
	byte_count = 0;
	byte_count2 = 0;

	this->bufferA = new char[BUFSIZE * sizeof(char) + 1];
	this->bufferB = new char[BUFSIZE * sizeof(char) + 1];

	// posix_memalign((void**) &bufferA, BUFSIZE, BUFSIZE * sizeof(char) + 1);
	// posix_memalign((void**) &bufferB, BUFSIZE, BUFSIZE * sizeof(char) + 1);
	//posix_memalign((void**) &zeiger, (BUFSIZE), 512 * sizeof(char) + 1);

	fin.open(file, fstream::in);
	readTextData(true);

	zeiger = bufferA;
	buffer_ptr = 0;
}

void Buffer::readTextData(bool x) {
	if(x){
		delete[] bufferA;
		bufferA = new char[BUFSIZE * sizeof(char) + 1];

		fin.read(bufferA, BUFSIZE * sizeof(char));
		bufferA[fin.gcount()] = '\0';
	} else {
		delete[] bufferB;
		bufferB = new char[BUFSIZE * sizeof(char) + 1];

		fin.read(bufferB, BUFSIZE * sizeof(char));
		bufferB[fin.gcount()] = '\0';
	}
	if(fin.gcount() != BUFSIZE * sizeof(char)) {
		readAgain = false;
		if(x) {
			bufferA[fin.gcount() - 1] = 0x03;
			bufferA[fin.gcount()] = '\0';
		} else {
			bufferB[fin.gcount() - 1] = 0x03;
			bufferB[fin.gcount()] = '\0';
		}
	}
}

char Buffer::getChar() {
	char *temp = zeiger;
	if(*zeiger == 0x03) {
		return *temp;
	}
	if (*(zeiger + 1) == '\0') {
		if ((zeiger + 1) == &bufferA[BUFSIZE * sizeof(char)]) {
			zeiger = &bufferB[0];			// lade die zweite Hälfte neu;
			buffer_ptr = 1;
			counter = 0;
		} else if ((zeiger+1) == &bufferB[BUFSIZE * sizeof(char)]) {
			zeiger = &bufferA[0];			// lade die erste Hälfte neu;
			buffer_ptr = 0;
			counter = 0;
		}
	} else {
		zeiger++;
		counter++;
	}

	if(counter == BUFSIZE - 10) {
		if(buffer_ptr == 0){
			readTextData(false);
		} else {
			readTextData(true);
		}
	}
//	if(*temp == 0x03) {
//		cout << "temp is / 0x03" << endl;
//		exit(1);
//	}
	return *temp;

}
void Buffer::ungetChar(int amount) {
	for(int i = 0; i < amount; i++){
		if(buffer_ptr == 0) { //in bufferA
			if(zeiger == &bufferA[0]) {
				zeiger = &bufferB[BUFSIZE - 1];
			} else {
				zeiger--;
			}
		} else { //in bufferB
			if(zeiger == &bufferB[0]) {
				zeiger = &bufferA[BUFSIZE - 1];
			} else {
				zeiger--;
			}
		}
	}
}

Buffer::~Buffer() {
	// TODO Auto-generated destructor stub
	delete[] bufferA;
	delete[] bufferB;
}
