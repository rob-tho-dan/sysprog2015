/*
 * Token.h

 *
 *  Created on: 16.04.2015
 *      Author: danielmauch
 */

#include <string.h>
#include <fstream>

#ifndef TOKEN_H_
#include "Information.h"
#include "TokenType.h"
#define TOKEN_H_

class Token {
public:
	Token(TokenType type, Information* information, int row, int column);
	~Token();

	TokenType getType();
	void setType(TokenType type);
	Information * getInformation();

	int getRow();
	int getColumn();

	void printToken(std::ofstream *file);

	char const * getPrintType();
private:
	TokenType type;
	Information* information;
	int row;
	int column;
};

#endif /* TOKEN_H_ */
