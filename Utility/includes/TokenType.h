#ifndef TOKENTYPE_H_
#define TOKENTYPE_H_

enum class TokenType {
	ERROR,
	NUMBER,			// digit digit*
	IDENTIFIER,		// letter ( letter | digit )*
	PLUS,         	// '+'
	MINUS,        	// '-'
	STAR,         	// '*'
	EXCLAMATIONMARK,// '!'
	LBRACE,       	// '('
	RBRACE,       	// ')'
	EQUAL,       	// '='
	LESS,         	// '<'
	GREATER,      	// '>'
	LCURLY,       	// '{'
	RCURLY,       	// '}'
	SEMICOLON,    	// ';'
	WRITE,
	READ,
	AND,			// '&&'
	DP,				// ':'
	DEF,			// ':='
	LECK,			// '['
	RECK,			// ']'
	SPECIAL,		// neue Bezeichnung fuer '=:='
	IF,           	// 'if' / 'IF'
	ELSE,
	WHILE,        	// 'while' / 'WHILE'
	INT,          	// 'int'
	BOOLEAN,      	// 'boolean'
	VOID,         	// 'void'
	RETURN,       	// 'return'
	UNDEFINED,		// Initialwert
	ENDOFFILE,
	COMMENT
};

#endif
