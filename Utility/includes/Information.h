#ifndef INFORMATION_H_
#include "TokenType.h"
#include "../../Parser/includes/NodeType.h"

#define INFORMATION_H_

class Information {
public:
	Information(const char * lexem, TokenType type, int value);
	~Information();

	int getValue();
	TokenType getTokenType();
	const char * getLexem();
	void setNodeType(NodeType type);
	NodeType getNodeType();
private:
	NodeType nodeType;
	const char * lexem;
	TokenType type;
	int value;
};

#endif
