/*
 * Klasse, welche die Einträge in der Symboltabelle darstellen soll
 *
 * Implementierung in SymTabEntry.cpp
 *
 * Eintrag wird gefunden durch HashWert über Lexem
 *
 * beinhaltet:
 * - pointer auf InformationsContainer
 * -
 */

#ifndef SYMTABENTRY_H_
#define SYMTABENTRY_H_

#include "Information.h"

class SymTabEntry {
public:
	SymTabEntry(const char * lexem, Information * information);
	SymTabEntry();
	~SymTabEntry();

	SymTabEntry * getNextEntry();
	void setNextEntry(SymTabEntry * nextEntry);
	const char * getLexem();
	Information * getInformation();
private:
	Information* linkToInformation;
	const char * lexem;
	SymTabEntry * next;
};

#endif
