/*
 * Token.cpp
 *
 *  Created on: 16.04.2015
 *      Author: danielmauch
 */

#include "../includes/Token.h"
#include "../includes/TokenType.h"
#include <iostream>
#include <fstream>

using namespace std;

Token::Token(TokenType type, Information* information, int row, int column) {
	this->type = type;
	this->information = information;
	this->row = row;
	this->column = column;
}

Token::~Token() {

}

TokenType Token::getType() {
	return Token::type;
}

void Token::setType(TokenType type) {
	this->type = type;
}

Information * Token::getInformation(){
	return this->information;
}

int Token::getRow(){
	return this->row;
}

int Token::getColumn(){
	return this->column;
}

void Token::printToken(ofstream *file){
	if(this->type != TokenType::ERROR) {
		*file << "Token: " << this->getPrintType() << "\t";
		if(this->type != TokenType::ENDOFFILE){
			*file << "Line: " << this->getRow() << "  \t";
			*file << "Column: " << this->getColumn() << "\t";

			if(this->type == TokenType::IDENTIFIER){
				*file << "Lexem: " << this->information->getLexem();
			}
			if(this->type == TokenType::NUMBER){
				*file << "Value: " << this->getInformation()->getValue();
			}
			if(this->type == TokenType::COMMENT){
				*file << "Comment: " << this->getInformation()->getLexem();
			}
		}
		*file << '\n';
	} else {
		if(this->type == TokenType::ERROR){
			fprintf(stderr, "Unknown Token Line: %d Column: %d Symbol: '%s'.\n",
			this->getRow(), this->getColumn(), this->getInformation()->getLexem());
		}
	}
}
char const * Token::getPrintType(){

	char const * help;

	switch(this->type){
	case TokenType::AND:
		help = "AND              ";
		break;
	case TokenType::BOOLEAN:
		help = "BOOLEAN          ";
		break;
	case TokenType::COMMENT:
		help = "COMMENT          ";
		break;
	case TokenType::DEF:
		help = "DEFINITION       ";
		break;
	case TokenType::DP:
		help = "COLON            ";
		break;
	case TokenType::ENDOFFILE:
		help = "ENDOFFILE        ";
		break;
	case TokenType::EQUAL:
		help = "EQUAL            ";
		break;
	case TokenType::ERROR:
		help = "ERROR            ";
		break;
	case TokenType::EXCLAMATIONMARK:
		help = "EXCLAMATIONMARK  ";
		break;
	case TokenType::GREATER:
		help = "GREATER          ";
		break;
	case TokenType::IDENTIFIER:
		help = "IDENTIFIER       ";
		break;
	case TokenType::IF:
		help = "IF               ";
		break;
	case TokenType::INT:
		help = "INTEGER          ";
		break;
	case TokenType::LBRACE:
		help = "LEFT BRACE       ";
		break;
	case TokenType::LCURLY:
		help = "LEFT CURLY BRACE ";
		break;
	case TokenType::LECK:
		help = "LEFT ECKIG       ";
		break;
	case TokenType::RECK:
		help = "RIGHT ECKIG      ";
		break;
	case TokenType::LESS:
		help = "LESS             ";
		break;
	case TokenType::MINUS:
		help = "MINUS            ";
		break;
	case TokenType::NUMBER:
		help = "NUMBER           ";
		break;
	case TokenType::PLUS:
		help = "PLUS             ";
		break;
	case TokenType::RBRACE:
		help = "RIGHT BRACE      ";
		break;
	case TokenType::RCURLY:
		help = "RIGHT CURLY BRACE";
		break;
	case TokenType::RETURN:
		if(*(this->information->getLexem()) == '\r') {
			help = "RETURN  r        ";
		} else {
			help = "RETURN  n        ";
		}
		break;
	case TokenType::SEMICOLON:
		help = "SEMICOLON        ";
		break;
	case TokenType::SPECIAL:
		help = "SPECIAL          ";
		break;
	case TokenType::STAR:
		help = "STAR             ";
		break;
	case TokenType::UNDEFINED:
		help = "UNDEFINED        ";
		break;
	case TokenType::VOID:
		help = "VOID             ";
		break;
	case TokenType::WHILE:
		help = "WHILE            ";
		break;
	case TokenType::WRITE:
		help = "WRITE            ";
		break;
	case TokenType::READ:
		help = "READ             ";
		break;
	case TokenType::ELSE:
		help = "ELSE             ";
		break;
	default:
		help = "ERROR            ";
		break;
	}

	return help;
}
