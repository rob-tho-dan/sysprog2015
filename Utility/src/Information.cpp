#include "../includes/Information.h"

Information::Information(const char * lexem, TokenType type, int value) {
	this->lexem = lexem;
	this->type = type;
	this->value = value;
}

Information::~Information() {

}

int Information::getValue() {
	return this->value;
}

TokenType Information::getTokenType() {
	return this->type;
}

const char * Information::getLexem() {
	return this->lexem;
}

void Information::setNodeType(NodeType type) {
	this->nodeType = type;
}

NodeType Information::getNodeType() {
	return this->nodeType;
}
