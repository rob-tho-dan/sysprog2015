#include <iostream>
#include <string.h>
#include <sstream>
#include "../includes/CodeGenerator.h"
#include "../includes/Node.h"

#define NULL __null

#include <iostream>
using namespace std;

CodeGenerator::CodeGenerator(const char * output_file) {
	code.open(output_file, std::fstream::in | std::fstream::out | std::fstream::trunc);
	amountLabels = 0;
}

CodeGenerator::~CodeGenerator(){
	code.close();
}

void CodeGenerator::makeCode(Node * node) {
	switch(node->rule()) {
		case Rule::PROG: {
			makeCode(node->decls());
			makeCode(node->statements());
			code << "STP\n";
			break;
		}

		case Rule::DECLS: {
			makeCode(node->decl());
			makeCode(node->nextDecl());
			break;
		}

		case Rule::DECL: {
			code << "DS $" << node->info()->getLexem();
			if(node->info()->getNodeType() != NodeType::INTEGER) {
				code << " " << *(node->array()->value()) << "\n";
			} else {
				code << " 1\n";
			}
			break;
		}

		case Rule::DECL_ARRAY: {
			//in Rule::DECL
			break;
		}

		case Rule::STATEMENTS: {
			makeCode(node->statement());
			makeCode(node->nextStatement());
			break;
		}

		case Rule::STATEMENT_IDENTIFIER: {
			makeCode(node->exp());
			code << "LA " << "$" << node->info()->getLexem() << "\n";
			makeCode(node->index());
			code << "STR\n";
			break;
		}

		case Rule::STATEMENT_WRITE: {
			makeCode(node->exp());
			code << "PRI\n";
			break;
		}
		case Rule::STATEMENT_READ: {
			code << "REA\n";
			code << "LA " << "$" << node->info()->getLexem();
			makeCode(node->index());
			code << "STR\n";
			break;
		}

		case Rule::STATEMENT_IF: {
			makeCode(node->exp());
			const char * if_label1 = newLabel();
			const char * if_label2 = newLabel();;
			code << "JIN #" << if_label1 << "\n"; //TODO LABEL1 ??
			makeCode(node->statement_if());
			//if(node->statement_else()->rule() != Rule::EMPTY) {
				code << "JMP #" << if_label2 << "\n"; //TODO LABEL2 ??
			//}
			code << "#" << if_label1 << " NOP\n";
			//if(node->statement_else()->rule() != Rule::EMPTY) {
				makeCode(node->statement_else());
				code << "#" << if_label2 << " NOP\n";
			//}
			break;
		}

		case Rule::STATEMENT_WHILE: {
			const char * while_label1 = newLabel();
			const char * while_label2 = newLabel();
			code << "#" << while_label1 << " NOP\n"; //TODO LABEL1
			makeCode(node->exp());
			code << "JIN #" << while_label2 << "\n"; //TODO LABEL2
			makeCode(node->statement());
			code << "JMP #" << while_label1 << "\n"; //TODO LABEL1
			code << "#" << while_label2 << " NOP\n";
			break;
		}

		case Rule::STATEMENT_BLOCK: {
			makeCode(node->statements());
			break;
		}

		case Rule::EXP: {
			if(node->op()->type() == NodeType::UNDEFINED) {
				makeCode(node->exp2());
			} else if(node->op()->op()->token()->getType() == TokenType::GREATER) {
				makeCode(node->op());
				makeCode(node->exp2());
				code << "LES\n";
			} else if(node->op()->op()->token()->getType() == TokenType::SPECIAL) {
				makeCode(node->exp2());
				makeCode(node->op());
				code << "NOT\n";
			} else {
				makeCode(node->exp2());
				makeCode(node->op());
			}
			break;
		}

		case Rule::EXP2: {
			makeCode(node->exp());
			break;
		}

		case Rule::EXP2_IDENTIFIER: {
			code << "LA $" << node->info()->getLexem() << "\n";
			makeCode(node->index());
			code << "LV\n";
			break;
		}

		case Rule::EXP2_INTEGER: {
			code << "LC " << *(node->value()) << "\n";
			break;
		}

		case Rule::EXP2_MINUS: {
			code << "LC 0\n";
			makeCode(node->exp2());
			code << "SUB\n";
			break;
		}

		case Rule::EXP2_NEGATION: {
			makeCode(node->exp2());
			code << "NOT\n";
			break;
		}

		case Rule::INDEX: {
			makeCode(node->exp());
			code << "ADD\n";
			break;
		}

		//INDEX EPSILON

		case Rule::OP_EXP: {
			makeCode(node->exp());
			makeCode(node->op());
			break;
		}

		//OP_EXP EPSILON

		case Rule::OP: {
			switch(node->token()->getInformation()->getTokenType()) {
				case TokenType::PLUS:
					code << "ADD\n";
					break;
				case TokenType::MINUS:
					code << "SUB\n";
					break;
				case TokenType::STAR:
					code << "MUL\n";
					break;
				case TokenType::DP:
					code << "DIV\n";
					break;
				case TokenType::LESS:
					code << "LES\n";
					break;
				case TokenType::GREATER:

					break;
				case TokenType::EQUAL:
					code << "EQU\n";
					break;
				case TokenType::SPECIAL:
					code << "EQU\n";
					break;
				case TokenType::AND:
					code << "AND\n";
					break;
				default:
					//not possible
					break;
			}
			break;
		}

		case Rule::EMPTY: {
			break;
		}

		default: {
			break;
		}
	}
}

void CodeGenerator::writeFile(){
	code.close();
}

const char * CodeGenerator::newLabel() {
	stringstream str;
  str << this->amountLabels;
	const char * stringLabels = str.str().c_str();
	char * tmp = new char[strlen("label")+strlen(stringLabels)+1];
  sprintf(tmp, "label%u", amountLabels);
  tmp[strlen("label")+strlen(stringLabels)+1] = 0;

	this->amountLabels++;

	return tmp;
}
