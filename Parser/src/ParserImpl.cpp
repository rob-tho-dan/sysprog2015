#include <iostream>
#include <string.h>
#include <sstream>
#include "../includes/ParserImpl.h"
#include "../../Utility/includes/Token.h"
#include "../includes/Node.h"
#include "../../Scanner/includes/Scanner.h"

#define NULL __null

#include <iostream>
using namespace std;

Parser * Parser::create(Scanner &scanner, Symboltable &symtab) {
	return (new ParserImpl(scanner, symtab));
}

ParserImpl::ParserImpl(Scanner &scanner, Symboltable &symtab):
	scanner(scanner), symtab(symtab) {
	this->currentToken = new Token(TokenType::UNDEFINED, NULL, 0, 0);
}

ParserImpl::~ParserImpl(){

}

Parser::~Parser(){

}

Node * ParserImpl::parse() {
	nextToken();
	return this->prog();
}

Node * ParserImpl::prog() {
	Node * decls = this->decls();
	Node * statements = this->statements();
	acceptToken(TokenType::ENDOFFILE);

	return new Node(Rule::PROG, decls, statements);
}

Node * ParserImpl::decls() {
	//Kommentar weglesen.
	if(this->currentToken->getType() == TokenType::COMMENT) {
		ParserImpl::nextToken();
	}

	if(this->currentToken->getType() != TokenType::INT) {
		return new Node(Rule::EMPTY);
	}

	Node * decl = this->decl();
	acceptToken(TokenType::SEMICOLON);
	return new Node(Rule::DECLS, decl, this->decls());
}

Node * ParserImpl::decl() {
	acceptToken(TokenType::INT);
	if(this->currentToken->getType() == TokenType::LECK) {
		Node * array = this->array();
		Information * identifierInfo = this->parseIdentifier();
		Node * temp = new Node(Rule::DECL, array, identifierInfo);
		temp->setColumn(this->currentToken->getColumn());
		temp->setRow(this->currentToken->getRow());
		return temp;
	} else {
		Information * identifierInfo = this->parseIdentifier();
		Node * temp = new Node(Rule::DECL, identifierInfo);
		temp->setColumn(this->currentToken->getColumn());
		temp->setRow(this->currentToken->getRow());
		return temp;
	}
}

Node * ParserImpl::array() {
	acceptToken(TokenType::LECK);
	unsigned value = this->parseInteger();
	acceptToken(TokenType::RECK);

	return new Node(Rule::DECL_ARRAY, value);
}

Node * ParserImpl::statements() {
	TokenType tmp = this->currentToken->getType();
	if(!(tmp == TokenType::IDENTIFIER || tmp == TokenType::WRITE
			|| tmp == TokenType::READ || tmp == TokenType::LCURLY
			|| tmp == TokenType::IF || tmp == TokenType::WHILE)) {
		return new Node(Rule::EMPTY);
	}

	Node * statement = this->statement();
	acceptToken(TokenType::SEMICOLON);
	return new Node(Rule::STATEMENTS, statement, this->statements());
}

Node * ParserImpl::statement() {
	Node * index;
	Node * exp;
	Information * info;
	Node * statements;
	Node * statement;
	Node * elseStatement;
	Node * temp;

	switch(this->currentToken->getType()) {
		case TokenType::IDENTIFIER:
			info = this->parseIdentifier();
			index = this->index();
			acceptToken(TokenType::DEF);
			exp = this->exp();
			temp = new Node(Rule::STATEMENT_IDENTIFIER, index, exp, info);
			temp->setRow(this->currentToken->getRow());
			temp->setColumn(this->currentToken->getColumn());
			return temp;

		case TokenType::WRITE:
			acceptToken(TokenType::WRITE);
			acceptToken(TokenType::LBRACE);
			exp = this->exp();
			acceptToken(TokenType::RBRACE);
			return new Node(Rule::STATEMENT_WRITE, exp);

		case TokenType::READ:
			acceptToken(TokenType::READ);
			acceptToken(TokenType::LBRACE);
			info = parseIdentifier();
			index = this->index();
			acceptToken(TokenType::RBRACE);
			return new Node(Rule::STATEMENT_READ, index, info);

		case TokenType::LCURLY:
			acceptToken(TokenType::LCURLY);
			statements = this->statements();
			acceptToken(TokenType::RCURLY);
			return new Node(Rule::STATEMENT_BLOCK, statements);

		case TokenType::IF:
			acceptToken(TokenType::IF);
			acceptToken(TokenType::LBRACE);
			exp = this->exp();
			acceptToken(TokenType::RBRACE);
			statement = this->statement();

			if(this->currentToken->getType() == TokenType::ELSE) {
				acceptToken(TokenType::ELSE);
				elseStatement = this->statement();
				return new Node(Rule::STATEMENT_IF, exp, statement, elseStatement);
			}
			return new Node(Rule::STATEMENT_IF, exp, statement, new Node(Rule::EMPTY));

		case TokenType::WHILE:
			acceptToken(TokenType::WHILE);
			acceptToken(TokenType::LBRACE);
		  exp = this->exp();
			acceptToken(TokenType::RBRACE);
			statement = this->statement();
			return new Node(Rule::STATEMENT_WHILE, exp, statement);

		case TokenType::COMMENT:
			ParserImpl::nextToken();
			return this->statement();

		default:
			//error Errorhandling
			error(this->currentToken->getType());
			return new Node(Rule::EMPTY);
	}
}

Node * ParserImpl::exp() {
	Node * exp2 = this->exp2();
	Node * op_exp = this->op_exp();
	return new Node(Rule::EXP, exp2, op_exp);
}

Node * ParserImpl::exp2() {
	Node * exp;
	Node * index;
	Information * info;
	unsigned value;
	Node * exp2;
	Node * temp;

	switch(this->currentToken->getType()) {
			case TokenType::LBRACE:
				acceptToken(TokenType::LBRACE);
				exp = this->exp();
				acceptToken(TokenType::RBRACE);
				return new Node(Rule::EXP2, exp);

			case TokenType::IDENTIFIER:
				info = this->parseIdentifier();
				index = this->index();
				if(index->rule() == Rule::EMPTY) {
					temp = new Node(Rule::EXP2_IDENTIFIER, index, info);
					temp->setColumn(this->currentToken->getColumn());
					temp->setRow(this->currentToken->getRow());
				}
				temp = new Node(Rule::EXP2_IDENTIFIER, index, info);
				temp->setColumn(this->currentToken->getColumn());
				temp->setRow(this->currentToken->getRow());
				return temp;

			case TokenType::NUMBER:
				value = this->parseInteger();
				return new Node(Rule::EXP2_INTEGER, value);

			case TokenType::MINUS:
				acceptToken(TokenType::MINUS);
				exp2 = this->exp2();
				return new Node(Rule::EXP2_MINUS, exp2);

			case TokenType::EXCLAMATIONMARK:
				acceptToken(TokenType::EXCLAMATIONMARK);
				exp2 = this->exp2();
				return new Node(Rule::EXP2_NEGATION, exp2);

			case TokenType::COMMENT:
				ParserImpl::nextToken();
				return this->exp2();

			default:
				error(this->currentToken->getType());
				return NULL;
	}
}

Node * ParserImpl::index() {
	if(this->currentToken->getType() != TokenType::LECK) {
		return new Node(Rule::EMPTY);
	}
	acceptToken(TokenType::LECK);
	Node * exp = this->exp();
	acceptToken(TokenType::RECK);
	return new Node(Rule::INDEX, exp);
}

Node * ParserImpl::op_exp() {
	TokenType tmp = this->currentToken->getType();
	if(!(tmp == TokenType::PLUS || tmp == TokenType::MINUS ||
			tmp == TokenType::STAR || tmp == TokenType::DP ||
			tmp == TokenType::LESS || tmp == TokenType::GREATER ||
			tmp == TokenType::EQUAL || tmp == TokenType::SPECIAL ||
			tmp == TokenType::AND)) {
		return new Node(Rule::EMPTY);
	}

	Node * op = this->op();
	Node * exp = this->exp();
	return new Node(Rule::OP_EXP, op, exp);
}

Node * ParserImpl::op() {
	TokenType tmp = this->currentToken->getType();
	if(!(tmp == TokenType::PLUS || tmp == TokenType::MINUS ||
			tmp == TokenType::STAR || tmp == TokenType::DP ||
			tmp == TokenType::LESS || tmp == TokenType::GREATER ||
			tmp == TokenType::EQUAL || tmp == TokenType::SPECIAL ||
			tmp == TokenType::AND)) {
		return new Node(Rule::EMPTY);
	}
	Node * op = new Node(Rule::OP, this->currentToken);
	nextToken();
	return op;
}

unsigned ParserImpl::parseInteger() {
	if(this->currentToken->getType() != TokenType::NUMBER) {
		error(this->currentToken->getType());
	}

	unsigned value = this->currentToken->getInformation()->getValue();
	acceptToken(TokenType::NUMBER);
	return value;
}

Information * ParserImpl::parseIdentifier() {
	if(this->currentToken->getType() != TokenType::IDENTIFIER) {
		error(this->currentToken->getType());
	}

	Information * info = this->currentToken->getInformation();

	acceptToken(TokenType::IDENTIFIER);
	return info;
}

void ParserImpl::acceptToken(TokenType type) {
	if(this->currentToken->getType() != type) {
		error(this->currentToken->getType());
	}

	//delete[] this->currentToken;
	if(this->currentToken->getType() != TokenType::ENDOFFILE) {
		ParserImpl::nextToken();
	}
}

void ParserImpl::nextToken() {
	this->currentToken = this->scanner.nextToken();
	if(this->currentToken->getType() == TokenType::RETURN || this->currentToken->getType() == TokenType::COMMENT) {
		this->nextToken();
	}
}

void ParserImpl::printTreeInXML(Node * node, int indent) {
	char * tabs = new char[indent + 1];
	for(int i = 0; i < indent; i++) {
		tabs[i] = ' ';
	}
	if(indent == 0)
		tabs = new char[1];
		tabs[0] = '-';

	switch(node->getAmountOfNodes()){
		case 0: {
			if(node->rule() == Rule::EMPTY) {
				//do nothing -> no error!

			} else if(node->rule() == Rule::OP) {
				std::cout << tabs << "<" << node->getRuleName() << " " << "operator = \"" << node->token()->getInformation()->getLexem() << "\"" << "/>" << endl;

			} else if(node->rule() == Rule::EXP2_INTEGER || node->rule() == Rule::DECL_ARRAY) {
				std::cout << tabs << "<" << node->getRuleName() << " " << "value = \"" << *(node->value()) << "\"" << "/>" << endl;

			} else if(node->rule() == Rule::DECL /*|| node->rule() == Rule::EXP2_IDENTIFIER*/) {
				std::cout << tabs << "<" << node->getRuleName() << " " << "identifier = \"" << node->info()->getLexem() << "\"" << "/>" << endl;

			} else {
				std::cout << "<ERROR />" << endl;

			}
			break;
		}

		case 1: {
			if(node->rule() == Rule::DECL) {
				std::cout << tabs << "<" << node->getRuleName() << " identifier = \"" << node->info()->getLexem() << "\">" << std::endl;
			} else if(node->rule() == Rule::EXP2_IDENTIFIER) {
				std::cout << tabs << "<" << node->getRuleName() << " " << "identifier = \"" << node->info()->getLexem() << "\"" << "/>" << endl;

			} else {
				std::cout << tabs << "<" << node->getRuleName() << ">" << std::endl;
			}
			if(node->rule() != Rule::EXP2_IDENTIFIER) {
				this->printTreeInXML(node->getFirstNode(), indent + 1);
				std::cout << tabs << "</" << node->getRuleName() << ">" << std::endl;
			}
			break;
		}

		case 2: {
			if(node->rule() == Rule::STATEMENT_IDENTIFIER) {
				std::cout << tabs << "<" << node->getRuleName() << " identifier = \"" << node->info()->getLexem() << "\">" << std::endl;
			} else {
				std::cout << tabs << "<" << node->getRuleName() << ">" << std::endl;
			}
			this->printTreeInXML(node->getFirstNode(), indent + 1);
			this->printTreeInXML(node->getSecondNode(), indent + 1);
			std::cout << tabs << "</" << node->getRuleName() << ">" << std::endl;
			break;
		}
		case 3: {
			std::cout << tabs << "<" << node->getRuleName() << ">" << std::endl;
			this->printTreeInXML(node->getFirstNode(), indent + 1);
			this->printTreeInXML(node->getSecondNode(), indent + 1);
			this->printTreeInXML(node->getThirdNode(), indent + 1);
			std::cout << tabs << "</" << node->getRuleName() << ">" << std::endl;
			break;
		}

		//Wird nie erreicht, da max. 3 Nodes vorhanden sind.
		default: {
			break;
		}
	}
}

void ParserImpl::error(TokenType type) {
	if(type != TokenType::ERROR) {
		fprintf(stderr, "Parsing fehlgeschlagen: Unerwartetes Token '%s' in Zeile '%d' und Spalte '%d'\n",
			getPrintType(type), this->currentToken->getRow(),
			this->currentToken->getColumn());
	} else {
		if((this->currentToken->getInformation()->getLexem()[0]) == ':' && (this->currentToken->getInformation()->getLexem())[1] == '*'){
			fprintf(stderr, "Unbeendeter Kommentar. Startet in Zeile '%d' und Spalte '%d'\n",
				this->currentToken->getRow(), this->currentToken->getColumn());
		} else {
			fprintf(stderr, "Unerwartetes Zeichen '%s' in Zeile '%d' und Spalte '%d'\n",
				this->currentToken->getInformation()->getLexem(), this->currentToken->getRow(),
				this->currentToken->getColumn());
		}
	}
	exit(1);
}

char const * ParserImpl::getPrintType(TokenType type){
	char const * help;

	switch(type){
	case TokenType::AND:
		help = "&&";
		break;
	case TokenType::BOOLEAN:
		help = "boolean";
		break;
	case TokenType::COMMENT:
		help = "Kommentar";
		break;
	case TokenType::DEF:
		help = ":=";
		break;
	case TokenType::DP:
		help = ":";
		break;
	case TokenType::ENDOFFILE:
		help = "EOF";
		break;
	case TokenType::EQUAL:
		help = "=";
		break;
	case TokenType::ERROR:
		help = "Error";
		break;
	case TokenType::EXCLAMATIONMARK:
		help = "!";
		break;
	case TokenType::GREATER:
		help = ">";
		break;
	case TokenType::IDENTIFIER:
		help = "Identifier";
		break;
	case TokenType::IF:
		help = "if";
		break;
	case TokenType::INT:
		help = "int";
		break;
	case TokenType::LBRACE:
		help = "(";
		break;
	case TokenType::LCURLY:
		help = "{";
		break;
	case TokenType::LECK:
		help = "[";
		break;
	case TokenType::RECK:
		help = "]";
		break;
	case TokenType::LESS:
		help = "<";
		break;
	case TokenType::MINUS:
		help = "Minus";
		break;
	case TokenType::NUMBER:
		help = "Nummer";
		break;
	case TokenType::PLUS:
		help = "+";
		break;
	case TokenType::RBRACE:
		help = "(";
		break;
	case TokenType::RCURLY:
		help = "}";
		break;
	case TokenType::RETURN:
		help = "return";
		break;
	case TokenType::SEMICOLON:
		help = ";";
		break;
	case TokenType::SPECIAL:
		help = "=:=";
		break;
	case TokenType::STAR:
		help = "*";
		break;
	case TokenType::UNDEFINED:
		help = "Undefiniert";
		break;
	case TokenType::VOID:
		help = "void";
		break;
	case TokenType::WHILE:
		help = "while";
		break;
	default:
		help = "Error";
		break;
	}

	return help;
}
