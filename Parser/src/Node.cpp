#include <iostream>
#include <string.h>
#include <sstream>
#include "../includes/Node.h"

#define NULL __null

#include <iostream>
using namespace std;

Node::Node(Rule rule):
  _rule(rule),
  _type(NodeType::UNDEFINED),
  _node1(nullptr),
  _node2(nullptr),
  _node3(nullptr),
  _info(nullptr),
  _value(0),
  _token(nullptr)
{}

Node::Node(Rule rule, Node * node):
  _rule(rule),
  _type(NodeType::UNDEFINED),
  _node1(node),
  _node2(nullptr),
  _node3(nullptr),
  _info(nullptr),
  _value(0),
  _token(nullptr)
{}

Node::Node(Rule rule, Node * node1, Node * node2):
  _rule(rule),
  _type(NodeType::UNDEFINED),
  _node1(node1),
  _node2(node2),
  _node3(nullptr),
  _info(nullptr),
  _value(0),
  _token(nullptr)
{}
Node::Node(Rule rule, Node * node1, Node * node2, Node * node3):
  _rule(rule),
  _type(NodeType::UNDEFINED),
  _node1(node1),
  _node2(node2),
  _node3(node3),
  _info(nullptr),
  _value(0),
  _token(nullptr)
{}
Node::Node(Rule rule, Information * info):
  _rule(rule),
  _type(NodeType::UNDEFINED),
  _node1(nullptr),
  _node2(nullptr),
  _node3(nullptr),
  _info(info),
  _value(0),
  _token(nullptr)
{}
Node::Node(Rule rule, Node * node, Information * info):
  _rule(rule),
  _type(NodeType::UNDEFINED),
  _node1(node),
  _node2(nullptr),
  _node3(nullptr),
  _info(info),
  _value(0),
  _token(nullptr)
{}
Node::Node(Rule rule, Node * node1, Node * node2, Information * info):
  _rule(rule),
  _type(NodeType::UNDEFINED),
  _node1(node1),
  _node2(node2),
  _node3(nullptr),
  _info(info),
  _value(0),
  _token(nullptr)
{}
Node::Node(Rule rule, unsigned value):
  _rule(rule),
  _type(NodeType::UNDEFINED),
  _node1(nullptr),
  _node2(nullptr),
  _node3(nullptr),
  _info(nullptr),
  _value(value),
  _token(nullptr)
{}
Node::Node(Rule rule, Token * token):
  _rule(rule),
  _type(NodeType::UNDEFINED),
  _node1(nullptr),
  _node2(nullptr),
  _node3(nullptr),
  _info(nullptr),
  _value(0),
  _token(token)
{}

Node::~Node(){}

NodeType Node::type() {
  return this->_type;
}

void Node::setType(NodeType type) {
  this->_type = type;
}

Rule Node::rule() {
  return this->_rule;
}

Node * Node::decl() {
  if(_rule == Rule::DECLS) {
    return _node1;
  } else {
    //TODO Errorhandling
    return nullptr;
  }
}

Node * Node::decls() {
  if(_rule == Rule::PROG) {
    return _node1;
  } else {
    //TODO Errorhandling
    return nullptr;
  }
}

Node * Node::statement() {
  if(_rule == Rule::STATEMENTS) {
    return _node1;
  } else if(_rule == Rule::STATEMENT_WHILE) {
    return _node2;
  } else {
    //TODO Errorhandling
    return nullptr;
  }
}

Node * Node::statements() {
  if(_rule == Rule::PROG) {
    return _node2;
  } else if(_rule == Rule::STATEMENT_BLOCK) {
    return _node1;
  } else {
    //TODO Errorhandling
    return nullptr;
  }
}

Node * Node::nextDecl() {
  if(_rule == Rule::DECLS) {
    return _node2;
  } else {
    //TODO Errorhandling
    return nullptr;
  }
}

Node * Node::nextStatement() {
  if(_rule == Rule::STATEMENTS) {
    return _node2;
  } else {
    //TODO Errorhandling
    return nullptr;
  }
}

Node * Node::array() {
  if(_rule == Rule::DECL) {
    return _node1;
  } else {
    //TODO Errorhandling
    return nullptr;
  }
}

Node * Node::exp() {
  if(_rule == Rule::STATEMENT_IDENTIFIER) {
    return _node2;
  } else if(_rule == Rule::STATEMENT_WRITE) {
    return _node1;
  } else if(_rule == Rule::STATEMENT_IF) {
    return _node1;
  } else if(_rule == Rule::STATEMENT_WHILE) {
    return _node1;
  } else if(_rule == Rule::EXP2) {
    return _node1;
  } else if(_rule == Rule::INDEX) {
    return _node1;
  } else if(_rule == Rule::OP_EXP) {
    return _node2;
  } else {
    //TODO Errorhandling
    return nullptr;
  }
}

Node * Node::exp2() {
  if(_rule == Rule::EXP || _rule == Rule::EXP2_MINUS || _rule == Rule::EXP2_NEGATION) {
    return _node1;
  } else {
    //TODO Errorhandling
    return nullptr;
  }
}

Node * Node::op() {
  if(_rule == Rule::EXP) {
    return _node2;
  } else if(_rule == Rule::OP_EXP){
    return _node1;
  } else {
    //TODO Errorhandling
    return nullptr;
  }
}

Node * Node::index() {
  if(_rule == Rule::STATEMENT_IDENTIFIER || _rule == Rule::STATEMENT_READ ||
  _rule == Rule::EXP2_IDENTIFIER) {
    return _node1;
  } else {
    //TODO Errorhandling
    return nullptr;
  }
}

Node * Node::statement_if() {
  if(_rule == Rule::STATEMENT_IF) {
    return _node2;
  } else {
    //TODO Errorhandling
    return nullptr;
  }
}

Node * Node::statement_else() {
  if(_rule == Rule::STATEMENT_IF) {
    return _node3;
  } else {
    //TODO Errorhandling
    return nullptr;
  }
}

Information * Node::info() {
  switch(_rule){
    case Rule::DECL:
    case Rule::DECL_ARRAY:
    case Rule::DECL_IDENTIFIER:
    case Rule::STATEMENT_IDENTIFIER:
    case Rule::STATEMENT_READ:
    case Rule::EXP2_IDENTIFIER:
      return _info;
    default:
      //TODO Errorhandling
      return nullptr;
  }
}

unsigned * Node::value() {
  if(_rule == Rule::DECL_ARRAY || _rule == Rule::EXP2_INTEGER) {
    return &_value;
  } else {
    //TODO Errorhandling
    return nullptr;
  }
}

Token * Node::token() {
  if(_rule == Rule::OP || _rule == Rule::STATEMENT_IDENTIFIER || _rule == Rule::EXP2_IDENTIFIER) {
    return _token;
  } else {
    //TODO Errorhandling
    return nullptr;
  }
}

const char * Node::getRuleName() {
  switch(_rule) {
    case Rule::PROG:
      return "PROG";
    case Rule::DECLS:
      return "DECLS";
    case Rule::DECL_ARRAY:
      return "DECL_ARRAY";
    case Rule::DECL_IDENTIFIER:
      return "DECL_IDENTIFIER";
    case Rule::STATEMENTS:
      return "STATEMENTS";
    case Rule::STATEMENT_IDENTIFIER:
      return "STATEMENT_IDENTIFIER";
    case Rule::STATEMENT_WRITE:
      return "STATEMENT_WRITE";
    case Rule::STATEMENT_READ:
      return "STATEMENT_READ";
    case Rule::STATEMENT_IF:
      return "STATEMENT_IF";
    case Rule::STATEMENT_WHILE:
      return "STATEMENT_WHILE";
    case Rule::STATEMENT_BLOCK:
      return "STATEMENT_BLOCK";
    case Rule::EXP:
      return "EXP";
    case Rule::EXP2:
      return "EXP2";
    case Rule::EXP2_IDENTIFIER:
      return "EXP2_IDENTIFIER";
    case Rule::EXP2_INTEGER:
      return "EXP2_INTEGER";
    case Rule::EXP2_MINUS:
      return "EXP2_MINUS";
    case Rule::EXP2_NEGATION:
      return "EXP2_NEGATION";
    case Rule::INDEX:
      return "INDEX";
    case Rule::OP_EXP:
      return "OP_EXP";
    case Rule::OP:
      return "OP";
    case Rule::EMPTY:
      return "EMPTY";
    case Rule::DECL:
      return "DECL";

    default:
      return nullptr;
  }
}

Node * Node::getFirstNode() {
  return _node1;
}

Node * Node::getSecondNode() {
  return _node2;
}

Node * Node::getThirdNode() {
  return _node3;
}

void Node::setRow(int row) {
  this->row = row;
}

void Node::setColumn(int column) {
  this->column = column;
}

int Node::getColumn() {
  return this->column;
}

int Node::getRow() {
  return this->row;
}

int Node::getAmountOfNodes() {
  unsigned i = 0;
  if(_node1 != nullptr) {
    i++;
  }
  if(_node2 != nullptr) {
    i++;
  }
  if(_node3 != nullptr) {
    i++;
  }

  return i;
}
