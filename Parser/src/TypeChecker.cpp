#include <iostream>
#include <string.h>
#include <sstream>
#include "../includes/TypeChecker.h"
#include "../includes/Node.h"

#define NULL __null

#include <iostream>
using namespace std;

void TypeChecker::typeCheck(Node * node) {
	switch(node->rule()) {
		case Rule::PROG:
			this->typeCheck(node->decls());
			this->typeCheck(node->statements());
			node->setType(NodeType::UNDEFINED);
			break;

		case Rule::DECLS:
			this->typeCheck(node->decl());
			this->typeCheck(node->nextDecl());
			break;

		case Rule::DECL:
			if(node->info()->getNodeType() != NodeType::UNDEFINED) {
				this->error("Identifier wurde schon deklariert.", node);
			} else {
				if(node->array() != nullptr) {
					this->typeCheck(node->array());
					node->info()->setNodeType(NodeType::ARRAY);
				} else {
					node->info()->setNodeType(NodeType::INTEGER);
				}
			}
			break;

		case Rule::DECL_ARRAY:
			if(*(node->value()) > 0) {
				node->setType(NodeType::ARRAY);
			} else {
				error("Kein gültiger Wert.", node);
			}
			break;

		case Rule::STATEMENTS:
			this->typeCheck(node->statement());
			this->typeCheck(node->nextStatement());
			break;

		case Rule::STATEMENT_IDENTIFIER:
			this->typeCheck(node->exp());
			this->typeCheck(node->index());
			if(node->info()->getNodeType() == NodeType::UNDEFINED) {
				this->error("Identifier ist nicht deklariert.", node);
			} else {
				if(!(node->exp()->type() == NodeType::INTEGER &&
					((node->info()->getNodeType() == NodeType::INTEGER && node->index()->type() == NodeType::UNDEFINED) ||
					(node->info()->getNodeType() == NodeType::ARRAY && node->index()->type() == NodeType::ARRAY)))) {
						error("Inkompatibler Typ.", node);
					}
			}
			break;

		case Rule::STATEMENT_WRITE:
			this->typeCheck(node->exp());
			break;

		case Rule::STATEMENT_READ:
			this->typeCheck(node->index());
			if(node->info()->getNodeType() == NodeType::UNDEFINED) {
				this->error("Identifier ist nicht deklariert.", node);
			} else {
				if(!((node->info()->getNodeType() == NodeType::INTEGER && node->index()->type() == NodeType::UNDEFINED) || (node->info()->getNodeType() == NodeType::ARRAY && node->index()->type() == NodeType::ARRAY))) {
					this->error("Inkompatibler Typ.", node);
				}
			}
			break;

		case Rule::STATEMENT_IF:
			this->typeCheck(node->exp());
			this->typeCheck(node->statement_if());
			this->typeCheck(node->statement_else());
			break;

		case Rule::STATEMENT_WHILE:
			this->typeCheck(node->exp());
			this->typeCheck(node->statement());
			break;

		case Rule::STATEMENT_BLOCK:
			this->typeCheck(node->statements());
			break;

		case Rule::EXP:
			this->typeCheck(node->exp2());
			this->typeCheck(node->op());
			if(node->op()->type() == NodeType::UNDEFINED) {
				node->setType(node->exp2()->type());
			} else if(node->op()->type() != node->exp2()->type()) {
				this->error("Operator ist nicht für Ausdruck definiert.", node);
			} else {
				node->setType(node->exp2()->type());
			}
			break;

		case Rule::EXP2:
			this->typeCheck(node->exp());
			node->setType(node->exp()->type());
			break;

		case Rule::EXP2_IDENTIFIER:
			this->typeCheck(node->index());
			if(node->info()->getNodeType() == NodeType::UNDEFINED) {
				error("Identifier ist nicht deklariert.", node);
			} else if(node->info()->getNodeType() == NodeType::INTEGER && node->index()->type() == NodeType::UNDEFINED) {
				node->setType(node->info()->getNodeType());
			} else if(node->info()->getNodeType() == NodeType::ARRAY && node->index()->type() == NodeType::ARRAY) {
				node->setType(NodeType::INTEGER);
			} else {
				this->error("Kein primitiver Typ.", node);
			}
			break;

		case Rule::EXP2_INTEGER:
			node->setType(NodeType::INTEGER);
			break;

		case Rule::EXP2_MINUS:
			this->typeCheck(node->exp2());
			node->setType(node->exp2()->type());
			break;

		case Rule::EXP2_NEGATION:
			this->typeCheck(node->exp2());
			if(node->exp2()->type() != NodeType::INTEGER) {
				this->error("Nur Integer können negiert werden.", node);
			} else {
				node->setType(NodeType::INTEGER);
			}
			break;

		case Rule::INDEX:
			this->typeCheck(node->exp());
			node->setType(NodeType::ARRAY);
			break;

		case Rule::OP_EXP:
			this->typeCheck(node->op());
			this->typeCheck(node->exp());
			node->setType(node->exp()->type());
			break;

		case Rule::OP:
			node->setType(NodeType::INTEGER);
			break;

		case Rule::EMPTY:
			break;

		// nicht moeglich
		default:
			break;
	}
}

void TypeChecker::error(const char * error, Node * node) {
	// if(node->token() != nullptr) {
		fprintf(stderr, "Typecheck fehlgeschlagen: '%s'. Line: %d Column %d.\n",
			error, node->getRow(), node->getColumn());
	// }
	exit(1);
}
