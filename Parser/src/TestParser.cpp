#include "../includes/Parser.h"
#include <iostream>
#include <string.h>
#include <sstream>
#include "../../Utility/includes/Token.h"
#include "../includes/Node.h"
#include "../../Scanner/includes/Scanner.h"
#include "../includes/TypeChecker.h"
#include "../includes/CodeGenerator.h"

class Scanner;

int main(int argc, char * argv[]) {
  //Random shit
  Parser * parser;
  Scanner * scanner;
  Buffer * buffer;
  TypeChecker * typeChecker = new TypeChecker();
  if(argv[2] != 0){
    if(argv[1] != 0) {
      buffer = new Buffer(argv[1]);
      Symboltable * symtab = new Symboltable();

      scanner = Scanner::create();
      scanner->update(buffer, symtab);
      parser = Parser::create(*scanner, *symtab);

      std::cout << std::endl << "parsing.." << std::endl;
      Node * tree = parser->parse();

      if(tree != nullptr) {
        if(argv[3] != 0 && *(argv[3]) == 'p') {
          std::cout << "printing tree in xml.." << std::endl;
          parser->printTreeInXML(tree, 0);
        }

        std::cout << "type checking.." << std::endl;
        typeChecker->typeCheck(tree);

        CodeGenerator * codeGen = new CodeGenerator(argv[2]);
        std::cout << "generating code.." << std::endl;
        codeGen->makeCode(tree);
        codeGen->writeFile();
      }
    }
  }
}
