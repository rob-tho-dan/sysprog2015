#ifndef PARSER_H_
#define PARSER_H_

#include "./Node.h"
#include "../../Scanner/includes/Scanner.h"

#include <iostream>
using namespace std;

class Node;

class Parser {
public:
	virtual ~Parser();
	static Parser * create(Scanner &scanner, Symboltable &symtab);

	virtual Node * parse() = 0;
	virtual void printTreeInXML(Node * node, int indent) = 0;
private:

};

#endif /* PARSER_H_ */
