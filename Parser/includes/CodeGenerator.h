#ifndef CODEGENERATOR_H_
#define CODEGENERATOR_H_

#include <iostream>
#include <fstream>
#include "../includes/Node.h"

using namespace std;
class CodeGenerator {
public:
  CodeGenerator(const char * output_file);
  ~CodeGenerator();
  void makeCode(Node * node); // Code Generierung
  void writeFile();

private:
  int amountLabels;
  std::fstream code;

  const char * newLabel();

};
#endif /* CODEGENERATOR_H_ */
