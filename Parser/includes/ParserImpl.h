#ifndef SCANNERIMPL_H_
#define SCANNERIMPL_H_
#include "Parser.h"
#include "../../Utility/includes/Token.h"

class ParserImpl: public Parser {
public:
	ParserImpl(Scanner &scanner, Symboltable &symtab);
	~ParserImpl();

	void nextToken();
	void acceptToken(TokenType);

	virtual Node * parse();
	virtual Node * prog();
	virtual Node * decls();
	virtual Node * decl();
	virtual Node * array();
	virtual Node * statements();
	virtual Node * statement();
	virtual Node * exp();
	virtual Node * exp2();
	virtual Node * index();
	virtual Node * op_exp();
	virtual Node * op();
	virtual unsigned parseInteger();
	virtual Information * parseIdentifier();

	virtual void printTreeInXML(Node * node, int indent);

	virtual void error(TokenType type);
	virtual char const * getPrintType(TokenType type);

private:
  int tmp;
	Token * currentToken;
	Scanner &scanner;
	Symboltable &symtab;

};

#endif
