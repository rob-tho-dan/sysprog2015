#ifndef NODETYPE_H_
#define NODETYPE_H_

enum class NodeType {
	INTEGER,
	ARRAY,
	UNDEFINED
};

#endif
