#ifndef NODE_H_
#define NODE_H_
#include "Parser.h"
#include "Rule.h"
#include "../../Utility/includes/Token.h"
#include "../../Utility/includes/Information.h"
#include "NodeType.h"

class Node {
public:
	//case 1
	explicit Node(Rule rule, Node * node); // alle mit einem children knoten -> index, exp2, exp2neg, write, block
	//case 2
	explicit Node(Rule rule, Node * node1, Node * node2); //alle mit zwei children -> prog, decls, statements, while
	//case 3
	explicit Node(Rule rule, Node * node1, Node * node2, Node * node3); //alle mit drei Children -> if

	//case 0
	explicit Node(Rule rule);
	explicit Node(Rule rule, Information * info); //DECL_IDENTIFIER
	explicit Node(Rule rule, unsigned value); // ARRAY, EXP2_INTEGER
	explicit Node(Rule rule, Token * token);

	//case 1
	explicit Node(Rule rule, Node * node, Information * info); // DECL_ARRAY, EXP2_IDENTIFIER, STATEMENT_READ

	//case 2
	explicit Node(Rule rule, Node * node1, Node * node2, Information * info); //STATEMENT_IDENTIFIER

	~Node();

	NodeType type();
	void setType(NodeType t);
	Node * decl();
	Node * decls();
	Node * statement();
	Node * statements();
	Node * nextDecl();
	Node * nextStatement();
	Node * array();
	Node * exp();
	Node * exp2();
	Node * op();
	Node * index();
	Node * statement_if();
	Node * statement_else();
	Information * info();
	unsigned * value();
	Token * token();
	Rule rule();
	int getAmountOfNodes();

	Node * getFirstNode();
	Node * getSecondNode();
	Node * getThirdNode();

	const char * getRuleName();

	void setRow(int row);
	void setColumn(int column);
	int getRow();
	int getColumn();
private:
  Rule _rule;
  Node * _node1;
	Node * _node2;
	Node * _node3;

	unsigned _value;
  Token * _token;
	Information * _info;
	NodeType _type;

	int row;
	int column;
};

#endif
