#ifndef TYPECHECKER_H_
#define TYPECHECKER_H_

#include <iostream>
#include "../includes/Node.h"

using namespace std;
class TypeChecker {
public:
  void typeCheck(Node * node); //TODO Semantische Analyse
private:
  void error(const char * error, Node * node);
};
#endif /* TYPECHECKER_H_ */
